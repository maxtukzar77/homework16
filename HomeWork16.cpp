#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
    const time_t t = time(NULL);    
    struct tm* lt = localtime(&t);
    int day = lt->tm_mday;
    
    const int N = 5;

    int mod = day % N;
    int count = 0;

    int arr[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << " ";
            if (i == mod)
                count += arr[i][j];
        }
        std::cout << "\n";
    }
    std::cout << "\n"  << count << "\n";

}